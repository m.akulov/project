window.addEventListener('DOMContentLoaded', function() {
    let adm = document.querySelectorAll('.adm');
    let header__ul = document.querySelectorAll('.header__ul-select');
    for (let i = 0; i < adm.length; ++i) {
        adm[i].addEventListener('mousemove', function() {
            header__ul[i].style.display = 'block';
        });
        adm[i].addEventListener('mouseout', function() {
            header__ul[i].style.display = 'none';
        });
    }
});